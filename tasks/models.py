from django.db import models
from projects.models import Project
from django.contrib.auth.models import User


# Create your models here.
class Task(models.Model):
    name = models.CharField(max_length=200)
    due_date = models.DateTimeField()
    start_date = models.DateTimeField()
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        on_delete=models.CASCADE,
        related_name="tasks",
    )
    assignee = models.ForeignKey(
        User, on_delete=models.CASCADE, related_name="tasks", null=True
    )
