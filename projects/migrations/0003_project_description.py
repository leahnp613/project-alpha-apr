# Generated by Django 4.2.2 on 2023-06-05 19:28

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0002_alter_project_owner"),
    ]

    operations = [
        migrations.AddField(
            model_name="project",
            name="description",
            field=models.TextField(default=""),
            preserve_default=False,
        ),
    ]
